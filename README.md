# ==<| RGB LED cube 5x5x5 |>==
## About
 * Simple documentation for users (not developers)

## Download
 * For downloading, please use GIT command with "--recursive" parameter

```bash
   $ git clone --recursive git@gitlab.com:RGB_LED_cube_5x5x5/DOC.git
```

## Help and support
 * If you have questions, or if you want to contribute on this project, just
   let me know [martin.stej at gmail dot com]